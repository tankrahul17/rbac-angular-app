Overview
This repository contains two projects: an Angular application and a NestJS application. Follow the instructions below to set up and run each project.

Angular Application
Directory: angular-app

Navigate to the Angular project directory: 
`cd angular-app`

Install the necessary packages: 
`npm install`

Start the Angular application: 
`ng serve`
The application will be available at http://localhost:4200.
 
Notes
Ensure you have Node.js and npm installed on your system before running the above commands.
If you encounter any issues, check the respective project documentation or raise an issue in the repository.
