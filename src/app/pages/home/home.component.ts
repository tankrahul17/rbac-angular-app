import { Component } from '@angular/core';
import { LocalStorageVars } from '../../utils/interfaces/constant.enum';
import { LocalStorageService } from '../../utils/services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  constructor(
    private router: Router,
    private readonly localStorageService: LocalStorageService
  ) {}

  ngOnInit(): void {
    const isLoggedIn:any =
      this.localStorageService.getItem(LocalStorageVars.accessTokenInfo);
        if(isLoggedIn){
          if(isLoggedIn?.role === 'admin') {
            this.router.navigateByUrl('landing/admin');
          } else if(isLoggedIn?.role === 'user') {
            this.router.navigateByUrl('landing/user');
          }
        }
  }
}
