import { DataSource } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, fromEvent, map, merge } from 'rxjs';
import { UserData } from '../../../utils/interfaces/user.interface';
import { DataService } from '../../../utils/services/data.service';
import { SingupComponent } from '../../auth/singup/singup.component';
import { DeleteDialogComponent } from '../../dialogs/delete/delete.dialog.component';
import { AuthService } from '../../../utils/services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WebSocketService } from '../../../utils/services/web-socket.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.scss',
})
export class AdminComponent {
  displayedColumns = ['_id', 'email', 'role', 'actions'];
  userDatabase!: any;
  dataSource!: any;
  index!: number;
  _id!: number;
  activeUsers$ = this.socketService.activeUsers$;

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public dataService: DataService,
    private _snackBar: MatSnackBar,
    public socketService: WebSocketService,
    public authService: AuthService
  ) {}

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;
  @ViewChild('filter', { static: true }) filter!: ElementRef;

  ngOnInit() {
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addNew() {
    const dialogRef = this.dialog.open(SingupComponent, {
      data: { adminFlag: true },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        this.refresh();
      }
    });
  }

  startEdit(
    i: number,
    _id: number,
    email: string,
    password: string,
    role: string
  ) {
    this._id = _id;
    this.index = i;
    const dialogRef = this.dialog.open(SingupComponent, {
      data: {
        _id: _id,
        email: email,
        password: password,
        role: role,
        editFlag: true,
        adminFlag: true,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        this._snackBar.open('User updated succesfully!', 'X', {
          duration: 3000,
          panelClass: ['mat-toolbar', 'mat-accent'],
        });
        this.refresh();
      }
    });
  }

  async deleteItem(
    i: number,
    _id: number,
    email: string,
    role: string,
    password: string
  ) {
    this.index = i;
    this._id = _id;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { _id: _id, email: email, password: password, role: role },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 1) {
        this._snackBar.open('User deleted succesfully!', 'X', {
          duration: 3000,
          panelClass: ['mat-toolbar', 'mat-accent'],
        });
        this.loadData();
      }
    });
  }

  public loadData() {
    this.userDatabase = new DataService(this.httpClient);
    this.dataSource = new UserDataSource(
      this.userDatabase,
      this.paginator,
      this.sort
    );
    fromEvent(this.filter.nativeElement, 'keyup')
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}

export class UserDataSource extends DataSource<UserData> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: UserData[] = [];
  renderedData: UserData[] = [];

  constructor(
    public _userDatabase: DataService,
    public _paginator: MatPaginator,
    public _sort: MatSort
  ) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => (this._paginator.pageIndex = 0));
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<UserData[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._userDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page,
    ];

    this._userDatabase.getAllUsers();

    return merge(...displayDataChanges).pipe(
      map(() => {
        // Filter data
        this.filteredData = this._userDatabase.data
          .slice()
          .filter((issue: any) => {
            const searchStr = (
              issue._id +
              issue.email +
              issue.role +
              issue.password
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });

        // Sort filtered data
        const sortedData = this.sortData(this.filteredData.slice());

        // Grab the page's slice of the filtered sorted data.
        const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
        this.renderedData = sortedData.splice(
          startIndex,
          this._paginator.pageSize
        );
        return this.renderedData;
      })
    );
  }

  disconnect() {}

  /** Returns a sorted copy of the database data. */
  sortData(data: UserData[]): UserData[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: any = '';
      let propertyB: any = '';

      switch (this._sort.active) {
        case '_id':
          [propertyA, propertyB] = [a._id, b._id];
          break;
        case 'email':
          [propertyA, propertyB] = [a.email, b.email];
          break;
        case 'role':
          [propertyA, propertyB] = [a.role, b.role];
          break;
        case 'password':
          [propertyA, propertyB] = [a.password, b.password];
          break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
