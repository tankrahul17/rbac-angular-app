import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../../utils/services/auth.service';
import { WebSocketService } from '../../../utils/services/web-socket.service';
import { LocalStorageService } from '../../../utils/services/local-storage.service';
import { LocalStorageVars } from '../../../utils/interfaces/constant.enum';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.scss',
})
export class UserComponent implements OnInit, OnDestroy {
  userName = '';
  interval: any;
  constructor(
    public authService: AuthService,
    private service: WebSocketService,
    private readonly localStorageService: LocalStorageService
  ) {}
  public ngOnInit() {
    const isLoggedIn: any = this.localStorageService.getItem(
      LocalStorageVars.accessTokenInfo
    );
    if (isLoggedIn) {
      this.userName = 'User ' + Math.floor(1000 + Math.random() * 9000);
    }
    this.interval = setInterval(() => {
      this.service.userPing(this.userName);
    }, 1000);
  }

  ngOnDestroy(): void {
    this.clearScoket();
  }

  clearScoket() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
}
