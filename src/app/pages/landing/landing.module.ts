import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../../angular-material.module';
import { DataService } from '../../utils/services/data.service';
import { DeleteDialogComponent } from '../dialogs/delete/delete.dialog.component';
import { AdminComponent } from './admin/admin.component';
import { LandingRoutingModule } from './landing-routing.module';
import { UserComponent } from './user/user.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WebSocketService } from '../../utils/services/web-socket.service';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:3000', options: {} };

@NgModule({
  declarations: [UserComponent, AdminComponent, DeleteDialogComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    AngularMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule.forRoot(config),
  ],
  providers: [DataService, WebSocketService],
})
export class LandingModule {}
