import { HttpErrorResponse } from '@angular/common/http';
import { Component, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ILoginResponse } from '../../../utils/interfaces/auth.interfaces';
import { LocalStorageVars } from '../../../utils/interfaces/constant.enum';
import { AuthService } from '../../../utils/services/auth.service';
import { LocalStorageService } from '../../../utils/services/local-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent {
  loginForm!: FormGroup;
  alert?: { message: string; type: 'error' | 'info' };
  isLoading = false;
  isLoggedIn = false;

  constructor(
    private readonly authService: AuthService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private readonly localStorageService: LocalStorageService
  ) {}

  /**
   * Initialize the login form
   */
  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
      ]),
    });
    this.isLoggedIn =
      this.localStorageService.getItem(LocalStorageVars.accessTokenInfo);
      if(this.isLoggedIn){
        this.router.navigateByUrl('home');
      }
  }

  /**
   *  Sign in the user with the credentials from the form.
   */
  async onSubmit(): Promise<void> {
    if (this.loginEnabled()) {
      console.warn('Tried to submit an invalid/dirty login form');
      return;
    }
    this.isLoading = true;
    const email = this.loginForm.get('email')?.value;
    const password = this.loginForm.get('password')?.value;
    this.authService.login({ email: email, password: password }).subscribe({
      next: (response) => {
        this._snackBar.open('Welcome Back!!', 'X', {
          duration: 3000,
          panelClass: ['mat-toolbar', 'mat-accent'],
        });
        this.isLoading = false;
        this.alert = {
          message: `Logged in as ${email}:${password}. Role: ${response.role}`,
          type: 'info',
        };
        this.authService.saveAccessInfo({
          token: response.token,
          role: response.role,
        });
        this.router.navigateByUrl('home');
      },
      error: (err: HttpErrorResponse) => {
        console.error(err);
        this.isLoading = false;
        this.alert = {
          message:
            err.status === 401
              ? `Failed to log in due to invalid credentials`
              : err?.error?.message,
          type: 'error',
        };
        this._snackBar.open(this.alert?.message, 'X', {
          duration: 3000,
          panelClass: ['mat-toolbar', 'mat-warn'],
        });
      },
    });
  }

  /**
   * Check whether the login forms' state allows logging in.
   * @returns whether it is possible to log in.
   */
  loginEnabled(): boolean {
    return !(this.isLoading || (this.loginForm.valid && this.loginForm.dirty));
  }

  /**
   * Allow pressing the log in button by pressing enter while the credentials are valid.
   */
  @HostListener('document:keydown.enter') enterKeyPressed() {
    if (this.loginEnabled()) {
      this.onSubmit();
    }
  }

  getAccessInfo(): ILoginResponse | null {
    return this.authService.getAccessInfo();
  }
}
