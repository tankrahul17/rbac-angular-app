import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Inject,
  OnInit,
  Optional,
  signal,
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ILoginResponse } from '../../../utils/interfaces/auth.interfaces';
import {
  Role,
  LocalStorageVars,
} from '../../../utils/interfaces/constant.enum';
import { AuthService } from '../../../utils/services/auth.service';
import { LocalStorageService } from '../../../utils/services/local-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrl: './singup.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SingupComponent implements OnInit {
  Roles: any = ['user', 'admin'];

  signupForm!: FormGroup;
  alert?: { message: string; type: 'error' | 'info' };
  isLoading = signal(false);
  roleArray: string[];
  isLoggedIn = false;
  inputDataLng = 0;
  constructor(
    private readonly authService: AuthService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private cdr: ChangeDetectorRef,
    @Optional() public dialogRef: MatDialogRef<SingupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly localStorageService: LocalStorageService
  ) {
    this.roleArray = Object.keys(Role);
  }

  // This method marks the component for change detection.
  triggerChangeDetection() {
    this.cdr.markForCheck();
  }

  /**
   * Initialize the signup form
   */

  ngOnInit(): void {
    this.initializeForm();
    this.checkLoginStatus();
    this.setInputDataLength();
    this.setupFormBasedOnData();
  }

  private initializeForm(): void {
    const passwordValue = this.data ? 'temppassword' : '';

    this.signupForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl(passwordValue, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
      ]),
      role: new FormControl(Role.user),
    });
  }

  private checkLoginStatus(): void {
    this.isLoggedIn = this.localStorageService.getItem(
      LocalStorageVars.accessTokenInfo
    );

    if (this.isLoggedIn && (!this.inputDataLng || this.inputDataLng <= 0)) {
      this.router.navigateByUrl('home');
    }
  }

  private setInputDataLength(): void {
    this.inputDataLng = Object.keys(this.data).length;
  }

  private setupFormBasedOnData(): void {
    if (this.inputDataLng > 0) {
      if (!this.data?.editFlag) {
        this.signupForm.controls['password'].disable();
      }
      if (this.data?.editFlag) {
        this.signupForm.controls['email'].setValue(this.data.email);
        this.signupForm.controls['role'].setValue(this.data.role);
      }
    }
  }

  get titleandbtn() {
    if (this.inputDataLng > 0) {
      return this.data?.editFlag
        ? { title: 'Edit User', btn: 'Edit User' }
        : { title: 'Add User', btn: 'Add User' };
    }

    return { title: 'Register', btn: 'Sign-up' };
  }

  /**
   *  Sign in the user with the credentials from the form.
   */
  async onSubmit(): Promise<void> {
    if (this.signupEnabled()) {
      console.warn('Tried to submit an invalid/dirty signup form');
      return;
    }
    this.isLoading.set(true);
    const email = this.signupForm.get('email')?.value;
    const password = this.signupForm.get('password')?.value;
    const role = this.signupForm.get('role')?.value;
    this.authService
      .register({ email: email, password: password }, role)
      .subscribe({
        next: (response) => {
          let msg = 'Successfully singup!!';
          if (this.data) {
            msg = 'User added successfully!!';
          }
          this._snackBar.open(msg, 'X', {
            duration: 3000,
            panelClass: ['mat-toolbar', 'mat-accent'],
          });
          this.isLoading.set(false);
          if (!this.data || !this.data.adminFlag) {
            this.alert = {
              message: `Signed up as ${email}:${password}. Role: ${response.role}`,
              type: 'info',
            };
            this.authService.saveAccessInfo({
              token: response.token,
              role: response.role,
            });
            this.router.navigateByUrl('home');
          } else if (this.data?.adminFlag) {
            this.dialogRef.close(1);
          }
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          this.isLoading.set(false);
          this.alert = {
            message: err?.error?.message,
            type: 'error',
          };
          this._snackBar.open(this.alert?.message, 'X', {
            duration: 3000,
            panelClass: ['mat-toolbar', 'mat-warn'],
          });
        },
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getIsLoading() {
    return this.isLoading();
  }

  /**
   * Check whether the signup forms' state allows logging in.
   * @returns whether it is possible to log in.
   */
  signupEnabled(): boolean {
    return !(
      this.getIsLoading() ||
      (this.signupForm.valid && this.signupForm.dirty)
    );
  }

  /**
   * Allow pressing the log in button by pressing enter while the credentials are valid.
   */
  @HostListener('document:keydown.enter') enterKeyPressed() {
    if (this.signupEnabled()) {
      this.onSubmit();
    }
  }

  getAccessInfo(): ILoginResponse | null {
    return this.authService.getAccessInfo();
  }
}
