import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ChangeDetectionStrategy, Component, Inject, Optional } from '@angular/core';
import { DataService } from '../../../utils/services/data.service';

@Component({
  selector: 'app-delete.dialog',
  templateUrl: './delete.dialog.html',
  styleUrls: ['./delete.dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteDialogComponent {
  constructor(
    @Optional() public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dataService: DataService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  async confirmDelete(): Promise<void> {
   await this.dataService.deleteUser(this.data.email);
   this.dialogRef.close(1);
  }
}
