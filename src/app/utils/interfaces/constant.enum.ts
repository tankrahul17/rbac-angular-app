export enum Role {
  user = 'user',
  admin = 'admin',
}

export enum LocalStorageVars {
  accessTokenInfo = 'token',
  role = 'role',
}
