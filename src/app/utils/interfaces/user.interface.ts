export class UserData {
  _id: number | undefined;
  email: string | undefined;
  password: string | undefined;
  role: string | undefined;
}
