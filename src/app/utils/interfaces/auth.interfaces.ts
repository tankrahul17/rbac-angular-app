import { Role } from './constant.enum';

export interface ILoginRequestDto {
  email: string;
  password: string;
}

export interface ILoginResponse {
  token: string;
  role: Role;
}

export interface ISignupRequestDto {
  email: string;
  password: string;
}

export interface ISignupResponse {
  token: string;
  role: Role;
}
