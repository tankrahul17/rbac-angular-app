import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env } from '../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class DemoService {
  constructor(private http: HttpClient) {}

  /**
   * Fetch users confidential data;
   * @returns observable of the API request
   */
  fetchUserData(): Observable<any> {
    return this.http.get<any>(`${env.apiUrl}/api/user`, httpOptions);
  }

  /**
   * Fetch admins confidential data;
   * @returns observable of the API request
   */
  fetchAdminData(): Observable<any> {
    return this.http.get<any>(`${env.apiUrl}/api/admin`, httpOptions);
  }
}
