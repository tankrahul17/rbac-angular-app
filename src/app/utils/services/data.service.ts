import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment as env } from '../../../environments/environment';
import { UserData } from '../interfaces/user.interface';
const httpOptions = {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
@Injectable()
export class DataService {
  dataChange: BehaviorSubject<UserData[]> = new BehaviorSubject<UserData[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private httpClient: HttpClient) {}

  get data(): UserData[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllUsers(): void {
    this.httpClient.get(`${env.apiUrl}/user`, httpOptions).subscribe(
      (data: any) => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
    );
  }

  deleteUser(email: any): void {
    this.httpClient.delete(`${env.apiUrl}/user`, { body: { email } }).subscribe(
      (data) => {
        return data;
      },
      (err: HttpErrorResponse) => {
        return err;
      }
    );
  }
}
