import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { ILoginResponse } from '../interfaces/auth.interfaces';
import { Role } from '../interfaces/constant.enum';
import { AuthService } from '../services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const requiredRole: Role = route?.data['role'];

    if (!requiredRole) {
      // eslint-disable-next-line no-throw-literal
      throw `
        Roles array in the Route \`data\` property has to be provided for AuthGuard.
          Example:
          {
            path: 'profile',
            component: ProfileComponent,
            canActivate: [AuthGuard],
            data: {
              roles: ['ROLE_ADMIN],
            },
          },
        `;
    }

    // check if the user is authorized to view the given page
    const accessInfo: ILoginResponse | null = this.authService.getAccessInfo();
    if (accessInfo === null) {
      return false;
    }
    const userRole: Role = accessInfo.role;
    const isAuthorized = userRole === requiredRole;
    if (!isAuthorized) {
      this.authService.logout();
      this._snackBar.open('You are not authorized to access this page!', 'X', {
        duration: 5000,
        panelClass: ['mat-toolbar', 'mat-warn'],
      });
      this.router.navigate(['/auth/login']);
    }
    return isAuthorized;
  }
}
