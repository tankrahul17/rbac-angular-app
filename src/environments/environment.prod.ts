// production-specific environment
export const environment = {
  production: true,
  envName: 'production',
  apiUrl: 'http://localhost:3333',
};
